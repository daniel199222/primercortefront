// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'loginModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginModel _$LoginModelFromJson(Map<String, dynamic> json) {
  return LoginModel(
    idUsuario: json['idUsuario'] as int,
    username: json['username'] as String,
    password: json['password'] as String,
    token: json['jwtToken'] as String,
  );
}

Map<String, dynamic> _$LoginModelToJson(LoginModel instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('idUsuario', instance.idUsuario);
  writeNotNull('username', instance.username);
  writeNotNull('password', instance.password);
  writeNotNull('jwtToken', instance.token);
  return val;
}
