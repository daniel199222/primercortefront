import 'package:json_annotation/json_annotation.dart';

part 'buyModel.g.dart';

@JsonSerializable()
class BuyModel {
  final num idcompra;
  final String cuotas_compra;
  final String valor_compra;
  final String detallecompra;
  final String fecha_compra;

  const BuyModel(this.idcompra, this.valor_compra, this.cuotas_compra,
      this.detallecompra, this.fecha_compra);

  factory BuyModel.fromJson(Map<String, dynamic> json) => _$BuyModelFromJson(json);
  Map<String, dynamic> toJson() => _$BuyModelToJson(this);
}
