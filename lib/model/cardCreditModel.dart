import 'package:json_annotation/json_annotation.dart';

part 'cardCreditModel.g.dart';

@JsonSerializable()
class CardCreditModel {
  int idTarjeta;
  String nombre_alias;
  String banco;
  int fecha_corte;
  double tasa_interes;
  double cupo;

  CardCreditModel({
      this.idTarjeta,
      this.nombre_alias,
      this.banco,
      this.fecha_corte,
      this.cupo,
      this.tasa_interes});

      factory CardCreditModel.fromJson(Map<String, dynamic> json) =>_$CardCreditModelFromJson(json);
          Map<String, dynamic> toJson() => _$CardCreditModelToJson(this);

}
