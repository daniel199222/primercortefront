// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'buyModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BuyModel _$BuyModelFromJson(Map<String, dynamic> json) {
  return BuyModel(
    json['idcompra'] as num,
    json['valor_compra'] as String,
    json['cuotas_compra'] as String,
    json['detallecompra'] as String,
    json['fecha_compra'] as String,
  );
}

Map<String, dynamic> _$BuyModelToJson(BuyModel instance) => <String, dynamic>{
      'idcompra': instance.idcompra,
      'cuotas_compra': instance.cuotas_compra,
      'valor_compra': instance.valor_compra,
      'detallecompra': instance.detallecompra,
      'fecha_compra': instance.fecha_compra,
    };
