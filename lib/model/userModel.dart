import 'package:json_annotation/json_annotation.dart';

part 'userModel.g.dart';

@JsonSerializable(includeIfNull: false)
class UserModel {
  final String nombre;
  final String apellidos;
  final int identificacion;
  final String correo;

  @JsonKey(nullable: true)
  final String contrasenia;
  final int idusuario;

  UserModel({this.nombre, this.apellidos, this.identificacion, this.correo, this.contrasenia, this.idusuario});

  factory UserModel.fromJson(Map<String, dynamic> json) => _$UserModelFromJson(json);
  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}