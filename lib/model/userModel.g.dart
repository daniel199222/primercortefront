// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'userModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) {
  return UserModel(
    nombre: json['nombre'] as String,
    apellidos: json['apellidos'] as String,
    identificacion: json['identificacion'] as int,
    correo: json['correo'] as String,
    contrasenia: json['contrasenia'] as String,
    idusuario: json['idusuario'] as int,
  );
}

Map<String, dynamic> _$UserModelToJson(UserModel instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }
  writeNotNull('nombre_alias', instance.nombre);
  writeNotNull('banco', instance.apellidos);
  writeNotNull('cupo', instance.identificacion);
  writeNotNull('tasa_interes', instance.correo);
  writeNotNull('fecha_corte', instance.contrasenia);
  writeNotNull('idTarjeta', instance.idusuario);
  return val;
}
