// GENERATED CODE - DO NOT MODIFY BY HAND
part of 'cardCreditModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CardCreditModel _$CardCreditModelFromJson(Map<String, dynamic> json) {
  return CardCreditModel(
    idTarjeta: json['idTarjeta'] as int,
    nombre_alias: json['nombre_alias'] as String,
    cupo: json['cupo'] as double,
    tasa_interes: json['tasa_interes'] as double,
    fecha_corte: json['fecha_corte'] as int,
    banco: json['banco'] as String,
  );
}

Map<String, dynamic> _$CardCreditModelToJson(CardCreditModel instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('idTarjeta', instance.idTarjeta);
  writeNotNull('nombre_alias', instance.nombre_alias);
  writeNotNull('cupo', instance.cupo);
  writeNotNull('tasa_interes', instance.tasa_interes);
  writeNotNull('fecha_corte', instance.fecha_corte);
  writeNotNull('banco', instance.banco);
  return val;
}
