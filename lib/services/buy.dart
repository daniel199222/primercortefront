import 'dart:convert';
import 'package:tarjetas/common/HttpHandler.dart';
import 'package:tarjetas/model/buyModel.dart';

class BuyService {

  final String servicio = 'rest/compra';
    
    Future<List> getCompra() async {
      
      var request = servicio;

      final response = await HttpHandler().getJson(request);

      var data = json.decode(response.body);

      var list = data as List;
      List<BuyModel> listTmp = list.map((i) => BuyModel.fromJson(i)).toList();

      if (response.statusCode == 200) {
        return listTmp;
      } else {
        throw Exception('No se encontro la informacion de la compra solicitada.');
      }
    }

    Future<String> postCompra(BuyModel buyModel) async {
      final response = await HttpHandler().postUser(buyModel);
      if (response.statusCode == 200) {
        return 'Compra creada';
      } else {
        throw Exception(response.body);
      }
    }

    Future<String> putCompra(BuyModel buyModel) async {
      final response = await HttpHandler().putJson(servicio, buyModel);
      if (response.statusCode == 200) {
        return 'Compra Actualizada';
      } else {
        throw Exception(response.body);
      }
    }

    Future<String> deleteCompra(int idCompra) async {
      final response = await HttpHandler().deleteJson(servicio, idCompra);
      if (response.statusCode == 200) {
        return 'Compra eliminada';
      } else {
        throw Exception(response.body);
      }
    }
}
