import 'dart:convert';

import 'package:tarjetas/common/HttpHandler.dart';
import 'package:tarjetas/model/loginModel.dart';

class LoginService {
    
    Future<LoginModel> doLogin(LoginModel loginModel) async {
      
      final response = await HttpHandler().postLogin(loginModel.toJson());

      if (response.statusCode == 200) {
        return LoginModel.fromJson(json.decode(response.body));
      } else {
        throw Exception('Usuario y/o contraseña incorrecctos.');
      }
    }
}
