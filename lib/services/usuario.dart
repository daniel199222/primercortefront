import 'dart:convert';

import 'package:tarjetas/common/HttpHandler.dart';
import 'package:tarjetas/model/userModel.dart';

class UserService {

  final String servicio = 'rest/usuario';
    
    Future<UserModel> getUser(int idUsuario) async {
      
      var request = servicio + '/' + idUsuario.toString();

      final response = await HttpHandler().getJson(request);

      if (response.statusCode == 200) {
        return UserModel.fromJson(json.decode(response.body));
      } else {
        throw Exception('No se encontro la informacion del usuario.');
      }
    }

    Future<String> postUser(UserModel userModel) async {
      final response = await HttpHandler().postUser(userModel);
      if (response.statusCode == 200) {
        return 'Usuario creado';
      } else {
        throw Exception(response.body);
      }
    }

    Future<String> putUser(UserModel userModel) async {
      final response = await HttpHandler().putJson(servicio, userModel);
      if (response.statusCode == 200) {
        return 'Usuario Actualizado';
      } else {
        throw Exception(response.body);
      }
    }

    Future<String> deleteUser(int idUsuario) async {
      final response = await HttpHandler().deleteJson(servicio, idUsuario);
      if (response.statusCode == 200) {
        return 'Usuario eliminado';
      } else {
        throw Exception(response.body);
      }
    }
}
