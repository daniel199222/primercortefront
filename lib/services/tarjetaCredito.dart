import 'dart:convert';

import 'package:tarjetas/common/HttpHandler.dart';
import 'package:tarjetas/model/cardCreditModel.dart';

class CardCreditService {

  final String servicio = 'rest/tarjetacredito';

  Future<List> getTarjeta() async {
      var request = servicio;
      final response = await HttpHandler().getJson(request);
      var data = json.decode(response.body);

      var list = data as List;
      List<CardCreditModel> cardsList = list.map((i) => CardCreditModel.fromJson(i)).toList();

      if (response.statusCode == 200) {        
        return cardsList;
      } else {
        throw Exception('No se encontro la informacion de la Tarjeta.');
      }
  }
  
  Future<String> postTarjeta(CardCreditModel cardCreditModel) async {
      final response = await HttpHandler().postUser(cardCreditModel);
      if (response.statusCode == 200) {
        return 'Tarjeta Creada creado';
      } else {
        throw Exception(response.body);
      }
    }

   Future<String> putTarjeta(CardCreditModel cardCreditModel) async {
      final response = await HttpHandler().putJson(servicio, cardCreditModel);
      if (response.statusCode == 200) {
        return 'Tarjeta Credito Actualizado';
      } else {
        throw Exception(response.body);
      }
    }
    Future<String> deleteTarjeta(int idTarjeta) async {
      final response = await HttpHandler().deleteJson(servicio, idTarjeta);
      if (response.statusCode == 200) {
        return 'Tarjeta eliminado';
      } else {
        throw Exception(response.body);
      }
    }
}
