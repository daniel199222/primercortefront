import 'package:flutter/material.dart';
import 'package:tarjetas/services/buy.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tarjetas/model/buyModel.dart';

//Se crea la clase compra

class Buy extends StatefulWidget {
  /* Inicialmente se ingresan parametros estaticos 
    _buildList(){return <BuyModel>[const BuyModel(0001,'100000','2','Avance','4/9/2019')];}*/

  static const String routeName = '/buy';

  @override
  _Buy createState() => new _Buy();
}

class _Buy extends State<Buy> {
  final rValorCompraInputController = TextEditingController();
  final rCuotasComprasInputController = TextEditingController();
  final rDetalleCompraInputController = TextEditingController();
  final rFechaCompraInputController = TextEditingController();

  List<BuyModel> listCompras = new List<BuyModel>();

  @override
  void initState() {
    super.initState();
    getCompra();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text('COMPRAS')),
      body: new Listbuy(listCompras),

//boton flotante para la opcion crear nueva compra
      floatingActionButton: new FloatingActionButton(
        backgroundColor: Theme.of(context).accentColor,
        child: new Icon(Icons.add_circle, color: Colors.white),
        onPressed: () {
          Navigator.push(context,
              new MaterialPageRoute(builder: (context) => new CreateBuy()));
        },
      ),
    );
  }

  getCompra() async {
    List<BuyModel> tmpLists = await BuyService().getCompra();
    setState(() {
      listCompras = tmpLists;
    });
  }
}

//clase para la creacion de una nueva compra
class CreateBuy extends StatelessWidget {
  final rValorCompraInputController = TextEditingController();
  final rCuotasComprasInputController = TextEditingController();
  final rDetalleCompraInputController = TextEditingController();
  final rFechaCompraInputController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text('NUEVA COMPRA')),
      body: new Container(
        child: new Center(
          child: new Column(

//codigo crecion de una nueva compra
              children: <Widget>[
                new TextField(
                  decoration: new InputDecoration(
                    hintText: 'Ingrese el valor de la compra',
                    labelText: 'Valor de la Compra',
                  ),
                  controller: rValorCompraInputController,
                ),

                new TextField(
                  decoration: new InputDecoration(
                      hintText: 'Ingrese el numero de Cuotas diferidas',
                      labelText: 'Numero de Cuotas'),
                  controller: rCuotasComprasInputController,
                ),

                new TextField(
                  decoration: new InputDecoration(
                      hintText: 'Ingrese el detalle de la Compra',
                      labelText: 'Detalle de la Compra'),
                  controller: rDetalleCompraInputController,
                ),

                new TextField(
                  decoration: new InputDecoration(
                      hintText: 'Fecha (dd/mm/aa) de la compra',
                      labelText: 'Fecha (dd/mm/aa) de la compra'),
                  controller: rFechaCompraInputController,
                ),

//Botones de Una nueva Compra
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new Padding(
                        padding: new EdgeInsets.all(7.0),
                      ),
                      new RaisedButton(
                          color: Colors.blue,
                          textColor: Colors.white,
                          child: new Text(
                            'Guardar',
                            style: TextStyle(fontSize: 15),
                          ),
                          onPressed: () {}),
                      new RaisedButton(
                          color: Colors.redAccent,
                          textColor: Colors.white,
                          child: new Text(
                            'Cancelar',
                            style: TextStyle(fontSize: 15),
                          ),
                          onPressed: () {})
                    ])
              ]),
        ),
      ),
    );
  }

//Funcion Guardar compra
  _registerCompra() async {
    var buyModel = BuyModel(
        0,
        rValorCompraInputController.text,
        rCuotasComprasInputController.text,
        rDetalleCompraInputController.text,
        rFechaCompraInputController.text);
    await BuyService().postCompra(buyModel);
  }
}

//clase para la creacion de la lista de Compras
class Listbuy extends StatelessWidget {
  final List<BuyModel> _buy;
  const Listbuy(this._buy);
  @override
  Widget build(BuildContext context) {
    return new ListView(
      children: _buildListBuy(),
    );
  }

//metodo para recorrer la lista y poder mostrarla
  List<ItemBuy> _buildListBuy() {
    return _buy.map((buy) => new ItemBuy(buy)).toList();
  }
}

//classe para traer un usuario y agregarlo a la lista
class ItemBuy extends StatelessWidget {
  final BuyModel _buy;
  const ItemBuy(this._buy);
  @override
  Widget build(BuildContext context) {
    return new ListTile(
        leading: new CircleAvatar(child: new Icon(Icons.credit_card)),
        title: new Text(_buy.detallecompra),
        subtitle: new Text(_buy.valor_compra),
        onTap: () {
//funcion de precionar un item de la lista y poder que el lo direccione a la opcion de modificacion y eliminacion
          Navigator.push(context,
              new MaterialPageRoute(builder: (context) => new ScreenBuy(_buy)));
        });
  }
}

//clase para mostrar el item en la lista
class ScreenBuy extends StatefulWidget {
  final BuyModel _buy;
  const ScreenBuy(this._buy);
  @override
  _ScreenBuy createState() => new _ScreenBuy(_buy);
}

class _ScreenBuy extends State<ScreenBuy> {
//instancia para permitir modificar el textfield

  final rValorCompraInputController = TextEditingController();
  final rCuotasComprasInputController = TextEditingController();
  final rDetalleCompraInputController = TextEditingController();
  final rFechaCompraInputController = TextEditingController();

  final BuyModel _detalle;
  _ScreenBuy(this._detalle);

  @override
  void initState() {
    super.initState();

// inicializacion de el control para editar textfield
    rValorCompraInputController.text = _detalle.valor_compra;
    rCuotasComprasInputController.text = _detalle.cuotas_compra;
    rDetalleCompraInputController.text = _detalle.detallecompra;
    rFechaCompraInputController.text = _detalle.fecha_compra;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text('DETALLE DE COMPRA')),
      body: new Container(
        child: new Center(
          child: new Column(

//cuerpo del aplicativo para edicion y modificacion
              children: <Widget>[
                new TextField(
                  decoration: new InputDecoration(
                      hintText: 'Ingrese el valor de la compra',
                      labelText: 'Valor de la Compra'),
                  controller: rValorCompraInputController,
                ),
                new TextField(
                  decoration: new InputDecoration(
                      hintText: 'Ingrese el numero de Cuotas diferidas',
                      labelText: 'Numero de Cuotas'),
                  controller: rCuotasComprasInputController,
                ),
                new TextField(
                  decoration: new InputDecoration(
                      hintText: 'Ingrese el detalle de la Compra',
                      labelText: 'Detalle de la Compra'),
                  controller: rDetalleCompraInputController,
                ),
                new TextField(
                  decoration: new InputDecoration(
                      hintText: 'Ingrese La fecha de Compra',
                      labelText: 'Fecha (dd/mm/aa) de la compra'),
                  controller: rFechaCompraInputController,
                ),

// Creacion de los botones para Editar o Eliminar una compra
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    new Padding(
                      padding: new EdgeInsets.all(7.0),
                    ),
                    new RaisedButton(
                        color: Colors.blue,
                        textColor: Colors.white,
                        child: new Text(
                          'Actualizar',
                          style: TextStyle(fontSize: 15),
                        ),
                        onPressed: () {}),
                    new RaisedButton(
                        color: Colors.red,
                        textColor: Colors.white,
                        child: new Text(
                          'Eliminar',
                          style: TextStyle(fontSize: 15),
                        ),
                        onPressed: () {})
                  ],
                )
              ]),
        ),
      ),
    );
  }

  _updateCompra() async {
    var cardCreditModel = BuyModel(
        this._detalle.idcompra,
        rValorCompraInputController.text,
        rCuotasComprasInputController.text,
        rDetalleCompraInputController.text,
        rFechaCompraInputController.text);

    await BuyService().putCompra(cardCreditModel);
  }

  _deleteCompra() async {
    await BuyService().deleteCompra(this._detalle.idcompra);
    await Navigator.of(context).pushNamed('/buy');
  }
}
