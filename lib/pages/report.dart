import 'package:flutter/material.dart';

class Report extends StatelessWidget{
  static const String routeName='/report';
  @override
  Widget build(BuildContext context){
    return new Scaffold(
      appBar: new AppBar(
        title: new Text ('REPORTES')
      ),

      body: new Container(

      child: new Column(
      children: <Widget>[
          new Padding(padding: new EdgeInsets.all(7.0),),
            new RaisedButton(
              color: Colors.white70,
              textColor: Colors.black,
              child: new Text('Reporte Tarjetas',style: TextStyle(fontSize: 18),),
              onPressed: (){}
            ),
            new RaisedButton(
              color: Colors.white70,
              textColor: Colors.black,
              child: new Text('Reporte Compras',style: TextStyle(fontSize: 18),),
              onPressed: (){}
            )

      ],
      )
      )
    );
  }
}