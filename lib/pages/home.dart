import 'package:flutter/material.dart';


class Home extends StatefulWidget{
  static const String routeName='/home';
  @override
  _HomeState createState()=> new _HomeState();
}

class _HomeState extends State<Home>{

  Drawer _getDrawer(BuildContext context){
    var info = new AboutListTile(
      child: new Text('Informacion de la APP'),
      applicationIcon: new Icon (Icons.info_outline),
      applicationName: 'API TARJETAS',
      applicationVersion: 'v1.0.0',
      icon: new Icon (Icons.info),
    );
  ListTile _getItem (Icon icon, String description, String route){
    return new ListTile(
      leading: (icon),
      title: new Text (description),
      onTap: (){
        setState(() {
          Navigator.of(context).pushNamed(route);
        });
      },
    );
    }
  
  ListView listView = new ListView (children: <Widget>[
    _getItem(new Icon(Icons.home),'Inicio','/home'),
    _getItem(new Icon(Icons.contacts),'Usuario','/user'),
    _getItem(new Icon(Icons.credit_card),'Tarjetas de Credito','/cardCredit'),
    _getItem(new Icon(Icons.add_shopping_cart),'Compras','/buy'),
    _getItem(new Icon(Icons.calendar_today),'Reportes','/report'),
    _getItem(new Icon(Icons.exit_to_app),'Cerrar Sesion','/'),
    info
    ]);
    return new Drawer(
      child: listView,
    );
  }

  @override
  Widget build (BuildContext context){

  return new Scaffold(
    appBar: new AppBar(
      title: new Text ('Inicio'),
    ),
    drawer: _getDrawer(context),
    body: new Container(
      child: new Center(
        child: new Column(
        children:<Widget>[
          new Padding(padding: new EdgeInsets.all(15.0),),
          new Text('Bienvenidos a API Tarjetas',style: new TextStyle(fontSize: 30.0),)
         
        ]
        )
      )
    )
  );
  }
}