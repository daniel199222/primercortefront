import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tarjetas/model/userModel.dart';
import 'package:tarjetas/services/usuario.dart';

class User extends StatefulWidget {
  static const String routeName = '/user';

  @override
  _UserState createState() => new _UserState();
}

class _UserState extends State<User> {
  final _formKey = GlobalKey<FormState>();

  final rNombreInputController = TextEditingController();
  final rApellidoInputController = TextEditingController();
  final rIdentificacionInputController = TextEditingController();
  final rCorreoInputController = TextEditingController();
  final rPasswordInputController = TextEditingController();

  @override
  void dispose() {
    rNombreInputController.dispose();
    rApellidoInputController.dispose();
    rIdentificacionInputController.dispose();
    rCorreoInputController.dispose();
    rPasswordInputController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _getUser();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(title: new Text('Usuario')),
        body: new Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          decoration: BoxDecoration(color: Colors.white),
          child: ListView(children: <Widget>[
            Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Image.asset('assets/images/userr.png', width: 100),
                  Padding(
                      padding: EdgeInsets.all(8.0),
                      child: TextFormField(
                          decoration: InputDecoration(hintText: 'Nombres'),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Este campo es requerido.';
                            }
                          },
                          controller: rNombreInputController)),
                  Padding(
                      padding: EdgeInsets.all(8.0),
                      child: TextFormField(
                          decoration: InputDecoration(hintText: 'Apellidos'),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Este campo es requerido.';
                            }
                          },
                          controller: rApellidoInputController)),
                  Padding(
                      padding: EdgeInsets.all(8.0),
                      child: TextFormField(
                          decoration:
                              InputDecoration(hintText: 'Identificación'),
                          keyboardType: TextInputType.number,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Este campo es requerido.';
                            }
                          },
                          controller: rIdentificacionInputController)),
                  Padding(
                      padding: EdgeInsets.all(8.0),
                      child: TextFormField(
                          decoration: InputDecoration(hintText: 'Correo'),
                          readOnly: true,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Este campo es requerido.';
                            }
                          },
                          controller: rCorreoInputController)),
                  Padding(
                      padding: EdgeInsets.all(8.0),
                      child: TextFormField(
                          decoration: InputDecoration(hintText: 'Contraseña'),
                          obscureText: true,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Este campo es requerido.';
                            }
                          },
                          controller: rPasswordInputController)),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RaisedButton(
                      child: Text('Actualizar'),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          _updateUser();
                        }
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RaisedButton(
                      child: Text('Eliminar'),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          _deleteUser();
                        }
                      },
                    ),
                  )
                ],
              ),
            ),
          ]),
        ));
  }

  _getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int idUsuario = prefs.getInt('idUsuario');

    UserModel user = await UserService().getUser(idUsuario);

    rNombreInputController.text = user.nombre;
    rApellidoInputController.text = user.apellidos;
    rIdentificacionInputController.text = user.identificacion.toString();
    rCorreoInputController.text = user.correo;
    rPasswordInputController.text = '';
  }

  _updateUser() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    int idUsuario = prefs.getInt('idUsuario');

    var userModel = UserModel(nombre: rNombreInputController.text,
                              apellidos: rApellidoInputController.text,
                              identificacion: int.parse(rIdentificacionInputController.text),
                              correo: rCorreoInputController.text,
                              contrasenia: rPasswordInputController.text,
                              idusuario: idUsuario);

    await UserService().putUser(userModel);

    _getUser();
  }

  _deleteUser() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    int idUsuario = prefs.getInt('idUsuario');

    await UserService().deleteUser(idUsuario);

    await Navigator.of(context).pushNamed('/'); 
  }
}
