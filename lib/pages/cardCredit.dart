import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tarjetas/model/cardCreditModel.dart';
import 'package:tarjetas/services/tarjetaCredito.dart';

class CardCredit extends StatefulWidget {
  //ruta para poder funcionar la transicion entre paginas
  static const String routeName = '/cardCredit';

  @override
  _CardCredit createState() => new _CardCredit();
}

class _CardCredit extends State<CardCredit> {
  final rNombreAliasInputController = TextEditingController();
  final rBancoInputController = TextEditingController();
  final rCupoInputController = TextEditingController();
  final rTasaInteresInputController = TextEditingController();
  final rFechaCorteInputController = TextEditingController();
  final rIdTarjetaInputController = TextEditingController();

  List<CardCreditModel> listCards = new List<CardCreditModel>();

  @override
  void initState() {
    super.initState();
    _getTarjeta();
  }
/*
  @override
  void dispose() {
    rNombreAliasInputController.dispose();
    rBancoInputController.dispose();
    rCupoInputController.dispose();
    rTasaInteresInputController.dispose();
    rFechaCorteInputController.dispose();
    rIdTarjetaInputController.dispose();
    super.dispose();
  }*/

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text('Tarjetas de Credito')),
      //listar las tarjetas de credito
      body: new ListCardCredit(listCards),
      //boton flotante para la opcion crear.
      floatingActionButton: new FloatingActionButton(
        backgroundColor: Theme.of(context).accentColor,
        child: new Icon(Icons.add_circle, color: Colors.white),
        onPressed: () {
          Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => new CreateCardCredit()));
        },
      ),
    );
  }

  _getTarjeta() async {
    List<CardCreditModel> tmpLists = await CardCreditService().getTarjeta();
    setState(() {
      listCards = tmpLists;
    });
  }
}

//clase para la creacion de un nuevo item.
class CreateCardCredit extends StatelessWidget {
  TextEditingController rNombreAliasInputController = TextEditingController();
  TextEditingController rBancoInputController = TextEditingController();
  TextEditingController rCupoInputController = TextEditingController();
  TextEditingController rTasaInteresInputController = TextEditingController();
  TextEditingController rFechaCorteInputController = TextEditingController();
  String nombre;
  String banco;
  String fecha;
  String tasa;
  String cupo;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text('Tarjeta de Credito')),
      body: new Container(
        child: new Center(
          child: new Column(
            //cuerpo del aplicativo para crecion del item
            children: <Widget>[
              new TextField(
                  decoration: new InputDecoration(
                      hintText: 'Ingrese Nombre', labelText: 'Nombre'),
                  onSubmitted: (name){
                    nombre =name;
                  },),
                 //controller: rNombreAliasInputController),
              new TextField(
                  decoration: new InputDecoration(
                      hintText: 'Ingrese Banco', labelText: 'Banco'),
                  onSubmitted: (bank){
                    banco=bank;
                  },),
                  //controller: rBancoInputController),
              new TextField(
                  decoration: new InputDecoration(
                      hintText: 'Ingrese La el dia de corte',
                      labelText: 'Fecha de corte'),
                  onSubmitted: (date){
                    fecha=date;
                  },),
                  //controller: rFechaCorteInputController),
              new TextField(
                  decoration: new InputDecoration(
                      hintText: 'Ingrese La tasa de interes de la tarjeta',
                      labelText: 'Tasa de Interes'),
                  onSubmitted: (tas){
                    tasa=tas;
                  },),
                  //controller: rTasaInteresInputController),
              new TextField(
                  decoration: new InputDecoration(
                      hintText: 'Ingrese el cupo de la tarjeta',
                      labelText: 'cupo'),
                  onSubmitted: (cup){
                    cupo=cup;
                  },),
                  //controller: rCupoInputController),
              new RaisedButton(
                child: new Text('Guardar'),
                onPressed: () {
                  _registerTarjeta();
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new CardCredit()));
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  _registerTarjeta() async {
    var cardCreditModel = CardCreditModel(
        idTarjeta: 0,
        nombre_alias: nombre,
        banco: banco,
        fecha_corte: int.parse(fecha),
        cupo: double.parse(cupo),
        tasa_interes: double.parse(tasa));

    await CardCreditService().postTarjeta(cardCreditModel);
  }
}

//clase para la creacion de la lista de item
class ListCardCredit extends StatelessWidget {
  //instancia para identificar el modelo
  final List<CardCreditModel> _cardCredit;
  const ListCardCredit(this._cardCredit);
  @override
  Widget build(BuildContext context) {
    return new ListView(
      children: _buildListCardCredit(),
    );
  }

  //metodo para recorrer la lista y poder listarla
  List<ItemCardCredit> _buildListCardCredit() {
    return _cardCredit
        .map((cardCredit) => new ItemCardCredit(cardCredit))
        .toList();
  }
}

//classe para traer un usuario y agregarlo a la lista
class ItemCardCredit extends StatelessWidget {
  //instancia para identificar el modelo
  final CardCreditModel _cardCredit;
  const ItemCardCredit(this._cardCredit);
  @override
  Widget build(BuildContext context) {
    return new ListTile(
        leading: new CircleAvatar(child: new Icon(Icons.credit_card)),
        title: new Text(_cardCredit.nombre_alias),
        subtitle: new Text(_cardCredit.banco),
        onTap: () {
          //activar funcion de precio a un item de la lista y poder que el lo direccione a la opcion de modificacion y eliminacion
          Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => new ScreenCardCredit(_cardCredit)));
        });
  }
}

//clase para mostrar el item en la lista
class ScreenCardCredit extends StatefulWidget {
  //instancia para identificar el modelo
  final CardCreditModel _cardCredit;
  const ScreenCardCredit(this._cardCredit);
  @override
  _ScreenCardCredit createState() => new _ScreenCardCredit(_cardCredit);
}

class _ScreenCardCredit extends State<ScreenCardCredit> {
  //instancia para permitir modificar el textfield
  TextEditingController rNombreAliasInputController = TextEditingController();
  TextEditingController rBancoInputController = TextEditingController();
  TextEditingController rCupoInputController = TextEditingController();
  TextEditingController rTasaInteresInputController = TextEditingController();
  TextEditingController rFechaCorteInputController = TextEditingController();
  TextEditingController rIdTarjetaInputController = TextEditingController();
  //instancia para identificar el modelo
  final CardCreditModel _detalle;
  _ScreenCardCredit(this._detalle);

  @override
  void initState() {
    super.initState();
    // inicializacion de el control para editar textfield
    rNombreAliasInputController.text = _detalle.nombre_alias;
    rBancoInputController.text = _detalle.banco;
    rFechaCorteInputController.text = _detalle.fecha_corte.toString();
    rTasaInteresInputController.text = _detalle.tasa_interes.toString();
    rCupoInputController.text = _detalle.cupo.toString();
    rIdTarjetaInputController.text = _detalle.idTarjeta.toString();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text('Detalle de Tarjeta')),
      body: new Container(
        child: new Center(
          child: new Column(
            //cuerpo del aplicativo para edicion y modificacion
            children: <Widget>[
              new TextField(
                decoration: new InputDecoration(
                    hintText: 'Ingrese Nombre', labelText: 'Nombre'),
                controller: rNombreAliasInputController,
              ),
              new TextField(
                decoration: new InputDecoration(
                    hintText: 'Ingrese Banco', labelText: 'Banco'),
                controller: rBancoInputController,
              ),
              new TextField(
                decoration: new InputDecoration(
                    hintText: 'Ingrese La el dia de corte',
                    labelText: 'Fecha de corte'),
                controller: rFechaCorteInputController,
              ),
              new TextField(
                decoration: new InputDecoration(
                    hintText: 'Ingrese La tasa de interes de la tarjeta',
                    labelText: 'Tasa de Interes'),
                controller: rTasaInteresInputController,
              ),
              new TextField(
                decoration: new InputDecoration(
                    hintText: 'Ingrese el cupo de la tarjeta',
                    labelText: 'cupo'),
                controller: rCupoInputController,
              ),
              new RaisedButton(
                child: new Text('Editar'),
                onPressed: () {
                  _updateTarjeta();
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new CardCredit()));
                },
              ),
              new RaisedButton(
                  child: new Text('Eliminar'),
                  onPressed: () {
                    _deleteTarjeta();
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new CardCredit()));
                  })
            ],
          ),
        ),
      ),
    );
  }

  _updateTarjeta() async {
    int idCard = _detalle.idTarjeta;

    var cardCreditModel = CardCreditModel(
      nombre_alias: rNombreAliasInputController.text,
      banco: rBancoInputController.text,
      idTarjeta: idCard,
      fecha_corte: int.parse(rFechaCorteInputController.text),
      cupo: double.parse(rCupoInputController.text),
      tasa_interes: double.parse(rTasaInteresInputController.text),
    );
    await CardCreditService().putTarjeta(cardCreditModel);
  }

  _deleteTarjeta() async {

    int idTarjeta = (_detalle.idTarjeta);
    await CardCreditService().deleteTarjeta(idTarjeta);
    await Navigator.of(context).pushNamed('/cardCredit');
  }
}
