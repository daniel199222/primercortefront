import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tarjetas/model/loginModel.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tarjetas/model/userModel.dart';
import 'package:tarjetas/services/login.dart';
import 'package:tarjetas/services/usuario.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(title: new Text('API Tarjetas')),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        decoration: BoxDecoration(color: Colors.white),
        child: ListView(children: <Widget>[
          Image.asset(
            'assets/images/tarjetas.png',
            width: 400,
          ),
          FormLogin()
        ]),
      ),
    );
  }
}

// Crea un Form Widget
class FormLogin extends StatefulWidget {
  @override
  FormLoginState createState() {
    return FormLoginState();
  }
}

// Crea la correspondiente State class.
// Esta clase contiene todo lo relaccionado al formulario.
class FormLoginState extends State<FormLogin> {
  final _formKey = GlobalKey<FormState>();
  final _formRegisterKey = GlobalKey<FormState>();
  final emailInputController = TextEditingController();
  final passInputController = TextEditingController();

  final rNombreInputController = TextEditingController();
  final rApellidoInputController = TextEditingController();
  final rIdentificacionInputController = TextEditingController();
  final rCorreoInputController = TextEditingController();
  final rPasswordInputController = TextEditingController();

  @override
  void dispose() {
    emailInputController.dispose();
    passInputController.dispose();
    rNombreInputController.dispose();
    rApellidoInputController.dispose();
    rIdentificacionInputController.dispose();
    rCorreoInputController.dispose();
    rPasswordInputController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          createEmailInput(),
          createPasswordinput(),
          createLoginButton(context),
          createRegisterButton(context)
        ],
      ),
    );
  }

  Widget createEmailInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 30),
      child: TextFormField(
        decoration: InputDecoration(hintText: 'Usuario'),
        validator: (value){
          if (value.isEmpty) {
            return 'Por favor ingrese el usuario.';
          }
        },
        controller: emailInputController,
      ),
    );
  }

  Widget createPasswordinput() {
    return Padding(
      padding: const EdgeInsets.only(top: 30),
      child: TextFormField(
        decoration: InputDecoration(hintText: 'Contraseña'),
        obscureText: true,
        validator: (value){
          if (value.isEmpty) {
            return 'Por favor ingrese la contraseña.';
          }
        },
        controller: passInputController,
      ),
    );
  }

  Widget createLoginButton(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 32),
        child: RaisedButton(
          child: Text('Entrar'),
          onPressed: () {
            if (_formKey.currentState.validate()) {
              _validateUser(context)   ;       
            }            
          },
        ));
  }

  Widget createRegisterButton(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 32),
      child: RaisedButton(
        child: Text('Registrar'),
        onPressed: () {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Form(
                  key: _formRegisterKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: TextFormField(
                          decoration: InputDecoration(hintText: 'Nombres'),
                          validator: (value){
                            if (value.isEmpty) {
                              return 'Este campo es requerido.';
                            }
                          },
                          controller: rNombreInputController
                        )
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: TextFormField(
                          decoration: InputDecoration(hintText: 'Apellidos'),
                          validator: (value){
                            if (value.isEmpty) {
                              return 'Este campo es requerido.';
                            }
                          },
                          controller: rApellidoInputController
                        )
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: TextFormField(
                          decoration: InputDecoration(hintText: 'Identificación'),
                          keyboardType: TextInputType.number,
                          validator: (value){
                            if (value.isEmpty) {
                              return 'Este campo es requerido.';
                            }
                          },
                          controller: rIdentificacionInputController
                        )
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: TextFormField(
                          decoration: InputDecoration(hintText: 'Correo'),
                          validator: (value){
                            if (value.isEmpty) {
                              return 'Este campo es requerido.';
                            }
                          },
                          controller: rCorreoInputController
                        )
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: TextFormField(
                          decoration: InputDecoration(hintText: 'Contraseña'),
                          obscureText: true,
                          validator: (value){
                            if (value.isEmpty) {
                              return 'Este campo es requerido.';
                            }
                          },
                          controller: rPasswordInputController
                        )
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          child: Text("Enviar"),
                          onPressed: () {
                            if (_formRegisterKey.currentState.validate()) {
                              _registerUser();
                            }
                          },
                        ),
                      )
                    ],
                  ),
                ),
              );
            }
          );
        },
      ),
    );
  }

  _validateUser(BuildContext context) async {
    var loginModel = LoginModel(username: emailInputController.text, password: passInputController.text);
  
    LoginModel login = await LoginService().doLogin(loginModel);

    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('username', loginModel.username);
    await prefs.setInt('idUsuario', login.idUsuario);
    await prefs.setString('token', login.token);

    await Navigator.of(context).pushNamed('/home'); 
  }

  _registerUser() async {
    var userModel = UserModel(nombre: rNombreInputController.text,
                              apellidos: rApellidoInputController.text,
                              identificacion: int.parse(rIdentificacionInputController.text),
                              correo: rCorreoInputController.text,
                              contrasenia: rPasswordInputController.text);

    await UserService().postUser(userModel);
  }
}
