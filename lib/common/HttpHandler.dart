import 'dart:async';
import 'dart:convert' show json;
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class HttpHandler{


    //final String _baseUrl = 'ec2-3-15-183-251.us-east-2.compute.amazonaws.com:3000';
    //final String _baseUrl = '192.168.43.8:3000';

    final String _baseUrl = 'ec2-3-15-183-251.us-east-2.compute.amazonaws.com:3000';
    //final String _baseUrl = '192.168.43.31:3000';

    final String _tokenKeyword = 'Bearer ';

    Future<http.Response> postLogin(objBody) {
      var uri = new Uri.http(_baseUrl, 'rest/authenticate');
      return http.post(uri, 
                      headers: {HttpHeaders.contentTypeHeader: 'application/json'},
                      body: json.encoder.convert(objBody));
    }

    Future<http.Response> postUser(objBody) async {

      var uri = new Uri.http(_baseUrl, 'rest/register');
      return http.post(uri, 
                      headers: {
                        HttpHeaders.contentTypeHeader: 'application/json'
                      },
                      body: json.encoder.convert(objBody));
    }

    Future<http.Response> getJson(service) async {
      
      SharedPreferences prefs = await SharedPreferences.getInstance();
      var token = _tokenKeyword + prefs.getString('token');
      
      var uri = new Uri.http(_baseUrl, service);
      return http.get(uri, headers: {HttpHeaders.authorizationHeader: token});
    }

    Future<http.Response> postJson(service, objBody) async {

      SharedPreferences prefs = await SharedPreferences.getInstance();
      var token = _tokenKeyword + prefs.getString('token');      
      var idUsuario = prefs.getInt('idUsuario');
      var requestBody = objBody;
      // requestBody.idusuario = idUsuario;
      
      var uri = new Uri.http(_baseUrl, service);
      return http.post(uri, 
                      headers: {
                        HttpHeaders.authorizationHeader: token,
                        HttpHeaders.contentTypeHeader: 'application/json'
                      },
                      body: json.encoder.convert(requestBody));
    }

    Future<http.Response> putJson(service, objBody) async {

      SharedPreferences prefs = await SharedPreferences.getInstance();
      var token = _tokenKeyword + prefs.getString('token');      
      var idUsuario = prefs.getInt('idUsuario');
      var requestBody = objBody;
      // requestBody.idusuario = idUsuario;
      
      var uri = new Uri.http(_baseUrl, service);
      return http.post(uri, 
                      headers: {
                        HttpHeaders.authorizationHeader: token,
                        HttpHeaders.contentTypeHeader: 'application/json'
                      },
                      body: json.encoder.convert(requestBody));
    }

    Future<http.Response> deleteJson(service, codigo) async {

      SharedPreferences prefs = await SharedPreferences.getInstance();
      var token = _tokenKeyword + prefs.getString('token');
      var completeService = service + '/' + codigo.toString();
      
      var uri = new Uri.http(_baseUrl, completeService);
      return http.delete(uri, 
                      headers: {
                        HttpHeaders.authorizationHeader: token
                      });
    }
}